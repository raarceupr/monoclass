### Mono

Programa donde implementamos la clase Mono. Consiste de tres archivos:

1. `mono.h` : contiene la declaración de la clase Mono.
2. `mono.cpp` : contiene la implementación de los métodos de la clase Mono.
3. `main.cpp`: contiene la función main donde creamos objetos de clase Mono.