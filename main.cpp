
// incluyo porque usaremos cout...
#include <iostream>
// incluyo porque crearemos objetos de clase Mono
#include "mono.h"

using namespace std;

int main()
{
	// Creamos dos monos

    Mono a (1.0, 1.0, 1.0, "pepin");
    Mono b (0.0, 3.0, 2.0, "rosa");

    
    // Ilustramos como usar el metodo piso

    if ( b.enPiso() ) cout << b.getNombre() << " esta en piso." << endl;

    // Ilustramos como usar el metodo dist

    cout << "Distancia entre " << a.getNombre() << " y " <<
            b.getNombre() << " es " << a.dist(b) << endl;
    
    return 0;
}

