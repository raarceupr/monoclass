// Este file incluye las implementaciones de los metodos de la clase Mono
// Al escoger buenos nombres de funciones y de variables se hace innecesario
// documentar estas funciones.
//
// Un usuario de la clase Mono, no necesariamente tendría acceso a este archivo.


#include "mono.h"
#include <iostream>
#include <cmath>
using namespace std;

Mono::Mono() {
    x = y = z = 0;
    nombre = "John Doe";
}


Mono::Mono(double xx, double yy, double zz, string name) {
    setX(xx);
    setY(yy);
    setZ(zz);
    setNombre(name);
}

void Mono::setX(double xx) {
    if (xx < 0 || xx > 10) {
        cout << "Error asignando valor a x: " << xx << endl;
        exit(1);
    }
    else x = xx;
}

void Mono::setY(double yy) {
    if (yy < 0 || yy > 20) {
        cout << "Error asignando valor a y: " << yy << endl;
        exit(1);
    }
    else y = yy;

}

void Mono::setZ(double zz) {
    if (zz < 0 || zz > 10) {
        cout << "Error asignando valor a y: " << zz << endl;
        exit(1);
    }
    else z = zz;
}

void Mono::setNombre(string name) {
    if (name.length() < 2) {
        cout << "Por favor asigne un nombre de mas de un char." << endl;
        exit(1);
    }
    else nombre = name;
}

double Mono::getX() const { 
    return x; 
}

double Mono::getY() const { 
    return y; 
}

double Mono::getZ() const { 
    return z; 
}

string Mono::getNombre() const { 
    return nombre; 
}

bool Mono::enPiso() const { 
    return (z >= 0 && z <= 1); 
}

double Mono::dist(const Mono &m) const {
    return sqrt( pow(x - m.x, 2.0) + pow(y - m.y, 2.0) +
                 pow(z - m.z, 2.0) );
}
