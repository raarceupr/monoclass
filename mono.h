// En este archivo creamos la declaración de la clase Mono.
// Note que proveo explicaciones de lo que hacen algunas de las 
// funciones. Típicamente un usuario de la clase Mono, tendría acceso
// a este archivo.

#ifndef MONO_H
#define MONO_H

#include <string>

using namespace std;

class Mono
{
private:
    double x, y, z;
    string nombre;
public:

    // Constructores
    Mono();
    Mono(double xx, double yy, double zz, string name);

    // Setters y getters

    void setX(double xx);
    void setY(double yy);
    void setZ(double zz);
    void setNombre(string name);

    double getX() const;
    double getY() const;
    double getZ() const;
    string getNombre() const;

    // Devuelve true si el mono está en el piso 
    bool enPiso() const;

    // Devuelve la distancia entre el mono invocante y el pasado
    // por parámetro.
    double dist(const Mono &m) const;

};

#endif // MONO_H
